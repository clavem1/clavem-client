import { Component, OnInit, NgZone } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-evenements',
  templateUrl: './evenements.page.html',
  styleUrls: ['./evenements.page.scss'],
})
export class EvenementsPage implements OnInit {

  price = {};
  constructor(private platform: Platform) { }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.price = {'lower': 0, 'upper': 5000};
    })
  }

  onSelected() {
    console.log(this.price);
    
  }

  onSubmit(form: NgForm) {
    console.log(form);
  }
}

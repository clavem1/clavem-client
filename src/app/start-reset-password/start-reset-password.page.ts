import { Component, OnInit } from '@angular/core';
import result, { ResetPasswordGQL, UpdatePasswordGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-start-reset-password',
  templateUrl: './start-reset-password.page.html',
  styleUrls: ['./start-reset-password.page.scss'],
})
export class StartResetPasswordPage implements OnInit {
  email;
  token;
  password;
  resetCode;
  messageResult: { status: string; message: string };
  updateState = false;
  constructor(
    private readonly resetPasswordGQL: ResetPasswordGQL,
    private readonly updatePasswordGQL: UpdatePasswordGQL,
  ) { }

  ngOnInit() {
  }

  resetPassword() {
    this.resetPasswordGQL.fetch({ email: this.email }).subscribe(
      (result) => {
        if(!result.errors) {
          console.log(result.data)
          this.token = result.data.resetPassword;
          this.messageResult = { status: "success", message: "Un code vous a été envoyé par mail! Veuillez copier le code de réinitialisation et le coller dans le champ ci-dessous." };
        } else {
          console.warn(result.errors);
          this.token = null;
          this.messageResult = { status: "danger", message: "Email introuvable" };
        }
      },
      (error) => {
        console.warn(error);
        this.token = null;
        this.messageResult = { status: "danger", message: "Email introuvable" };
      }
    )
  }

  updatePassword() {
    this.updatePasswordGQL.mutate({ updatePasswordDto: { resetCode: parseInt(this.resetCode), password: this.password, resetToken: this.token  } }).subscribe(
      (result) => {
        if(!result.errors) {
          this.messageResult = { message: "Mot de passe modifié avec succés", status: "success" };
          this.updateState = true;
        } else {
          this.messageResult = { message: "Code invalide", status: "danger" }
        }
      },
      (errors) => {
        this.messageResult = { message: "Code invalide", status: "danger" }
      }
    )
  }
}

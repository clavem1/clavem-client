import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartResetPasswordPage } from './start-reset-password.page';

const routes: Routes = [
  {
    path: '',
    component: StartResetPasswordPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartResetPasswordPageRoutingModule {}

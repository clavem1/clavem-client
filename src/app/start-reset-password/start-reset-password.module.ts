import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartResetPasswordPageRoutingModule } from './start-reset-password-routing.module';

import { StartResetPasswordPage } from './start-reset-password.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StartResetPasswordPageRoutingModule
  ],
  declarations: [StartResetPasswordPage]
})
export class StartResetPasswordPageModule {}

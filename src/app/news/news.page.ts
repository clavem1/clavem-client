import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FetchActivePubsGQL, FetchUpActivePubsGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  upPub;
  pubs = [];
  baseAttachmentAPI = `${environment.API_URL}/attachment`;
  constructor(
    private readonly fetchActivePubsGQL: FetchActivePubsGQL,
    private readonly fetchUpActivePubsGQL: FetchUpActivePubsGQL,
  ) { }

  ngOnInit() {
    this.fetchActivePubsGQL.fetch({}).subscribe(
      (result) => {
        this.pubs = result.data.fetchActivePubs;
      }
    )

    this.fetchUpActivePubsGQL.fetch({}).subscribe(
      (result) => {
        const upPubs = result.data.fetchUpActivePubs;
        if(upPubs.length) {
          this.upPub = upPubs[0];
        }
      }
    )
  }

  generateUrl(url) {
    return this.baseAttachmentAPI + '/' + url;
  }

}

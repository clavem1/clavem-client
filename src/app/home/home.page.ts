import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { environment } from 'src/environments/environment';
import { FetchActivePubsGQL, FetchCategoriesGQL } from 'src/generated/graphql';
import { Router } from '@angular/router';
import { Platform, IonRouterOutlet } from '@ionic/angular';

@Component({
	selector: 'app-home',
	templateUrl: './home.page.html',
	styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
	news = true;
	discussion = false;
	categorie = true;
	preference = false;
	currentIndex = 0;
	categories = [];
	slideOpts = {
		initialSlide: 0,
		slidesPerView: 1,
		autoplay: true,
	};
	baseAttachmentAPI = `${environment.API_URL}/attachment`;
	pubs = [];
	constructor(
		private readonly fetchActivePubsGQL: FetchActivePubsGQL,
		private readonly fetchCategoriesGQL: FetchCategoriesGQL,
		private router: Router,
		private socialSharing: SocialSharing,
		private platform: Platform,
		private routerOutlet: IonRouterOutlet
	) {
		this.platform.backButton.subscribeWithPriority(0, () => {
			if (!this.routerOutlet.canGoBack()) {
				if (confirm("Voulez-vous quitter l'application ?")) {
					navigator['app'].exitApp();
				}
			}
		});
		this.toggleCategorie();
	}

	ngOnInit() {
		this.fetchActivePubsGQL.fetch({}).subscribe(
			(result) => {
				this.pubs = result.data.fetchActivePubs;
			}
		)
		this.fetchCategoriesGQL.fetch({}).subscribe(
			(result) => {
				this.categories = result.data.fetchCategories.records;
			},
			error => {
				alert('Error fetch categories');
			}
		)
	}

	onSlide(slides) {
		slides.getActiveIndex().then((index: number) => {
			this.currentIndex = index;
		});
	}
	toggleCategorie() {
		this.categorie = true;
		this.preference = false;
	}

	togglePreference() {
		this.router.navigate(['preferences']);
	}

	toggleNews() {
		this.news = true;
		this.discussion = false;
	}

	toggleDiscussion() {
		this.discussion = true;
		this.news = false;
	}

	generateUrl(url) {
		return this.baseAttachmentAPI + '/' + url;
	}

	shareYourCurrentApp() {
		let appUrl = "https://play.google.coom/store/apps/details?id=io.ionic.starter";
		this.socialSharing.share(null, null, null, appUrl).then(() => {

		}).catch((error) => {
			console.log(error);
		})
	}
}

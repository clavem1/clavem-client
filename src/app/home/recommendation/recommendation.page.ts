import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FetchCategoriesGQL, FetchEventsGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.page.html',
  styleUrls: ['./recommendation.page.scss'],
})
export class RecommendationPage implements OnInit {

  categorie = true;
  preference = false;
  categories = [];
  events = [];
  basePosterAPI = `${environment.API_URL}/event/poster`;

  constructor(
    private readonly fetchCategoriesGQL: FetchCategoriesGQL,
    private readonly fetchEventsGQL: FetchEventsGQL,
    ) { }

  ngOnInit() {
    this.fetchCategoriesGQL.fetch({}).subscribe(
      (result) => {
        this.categories = result.data.fetchCategories.records;
      },
      error => {
        console.log(JSON.stringify(error));
      }
    )

    this.fetchEventsGQL.fetch({}).subscribe(
      (result) => {
        this.events = result.data.fetchEvents.records;
      }
    )
  }

  toggleCategorie() {
    this.categorie = true;
    this.preference = false;
  }

  togglePreference() {
    this.preference = true;
    this.categorie = false;
  }

  generateUrl(url) {
    return this.basePosterAPI + '/' + url;
  }
}

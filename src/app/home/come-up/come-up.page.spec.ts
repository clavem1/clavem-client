import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ComeUpPage } from './come-up.page';

describe('ComeUpPage', () => {
  let component: ComeUpPage;
  let fixture: ComponentFixture<ComeUpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComeUpPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ComeUpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

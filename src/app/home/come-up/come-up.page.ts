import { Component, OnInit } from '@angular/core';
import { FetchCategoriesGQL, FetchRecentEventsGQL } from 'src/generated/graphql';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-come-up',
  templateUrl: './come-up.page.html',
  styleUrls: ['./come-up.page.scss'],
})
export class ComeUpPage implements OnInit {
  basePosterAPI = `${environment.API_URL}/event/poster`;
  categories = [];
  recentEvents = [];
  constructor(
    private readonly fetchCategoriesGQL: FetchCategoriesGQL,
    private readonly fetchRecentEventsGQL: FetchRecentEventsGQL,
    private router: Router
  ) { }

  ngOnInit() {
    this.fetchCategoriesGQL.fetch({}).subscribe(
      (result) => {
        this.categories = result.data.fetchCategories.records;
      },
      error => {
        alert('Error fetch categories');
      }
    )
    this.fetchRecentEventsGQL.fetch({}).subscribe(
      (result) => {
        this.recentEvents = result.data.fetchRecentEvents.records;
      },
      (error) => {
        alert('Error fetch recent events');
      }
    )
  }

  generateUrl(url) {
    return this.basePosterAPI + '/' + url;
  }
}

import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FetchCategoriesGQL, FetchEventsGQL } from 'src/generated/graphql';

@Component({
	selector: 'app-preferences',
	templateUrl: './preferences.page.html',
	styleUrls: ['./preferences.page.scss'],
})
export class PreferencesPage implements OnInit {
	price = {};
	eventList = [];
	events = [];
	categories = [];
	constructor(
		private platform: Platform,
		private readonly fetchEventsGQL: FetchEventsGQL,
		private readonly fetchCategoriesGQL: FetchCategoriesGQL,
	) { }
	basePosterAPI = `${environment.API_URL}/event/poster`;

	generateUrl(url) {
		return this.basePosterAPI + '/' + url;
	}
	ngOnInit() {
		this.platform.ready().then(() => {
			this.price = { lower: 0, upper: 50000 };
		});
		this.fetchEventsGQL.fetch({}).subscribe(
			(result) => {
				this.events = result.data.fetchEvents.records;
				this.eventList = this.events;
			}
		)
		this.fetchCategoriesGQL.fetch({}).subscribe(
			(result) => {
				this.categories = result.data.fetchCategories.records;
			}
		)
	}

	onSubmit(form: NgForm) {
		this.eventList = this.events;
		console.log(form);

		this.eventList = this.eventList.filter(event => {
			if (form['region'] !== "") {
				if (form['city'] !== "") {
					if (event.locationAccuracy.toLowerCase().match(form['city']) !== null) {
						return true
					}
				}
			}
			if (form['city'] !== "") {
				if (event.locationAccuracy.toLowerCase().match(form['city']) !== null) {
					return true
				}
			}
			if (event.tickets.length > 0) {
				for (let i = 0; i < 2; i++) {
					if (event.tickets[i] !== undefined) {
						if (event.tickets[i].price >= form["price"]['lower']) {
							if (event.tickets[i].price <= form["price"]['upper']) {
								if (new Date(event.startDate).getTime() >= new Date(form["dateDebut"]).getTime()) {
									if (new Date(event.startDate).getTime() <= new Date(form["dateFin"]).getTime()) {
										return true
									}
								}
								return true;
							}
						}
					}
				}
			}
			// if (new Date(event.startDate).getTime() >= new Date(form["dateDebut"]).getTime()) {
			// 	if (new Date(event.startDate).getTime() <= new Date(form["dateFin"]).getTime()) {
			// 		return true
			// 	}
			// }
		});
	}
}

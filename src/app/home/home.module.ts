import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { NewsPage } from './news/news.page';
import { DiscussionPage } from './discussion/discussion.page';
import { ComeUpPage } from './come-up/come-up.page';
import { RecommendationPage } from './recommendation/recommendation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
  ],
  declarations: [HomePage, NewsPage, DiscussionPage, ComeUpPage, RecommendationPage]
})
export class HomePageModule { }

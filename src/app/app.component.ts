import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RouterEvent, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  pages = [
    {
      title: 'Accueil',
      url: 'home',
      icon: 'home-outline'
    },
    {
      title: 'Ma Carte',
      url: 'cards',
      icon: 'card-outline'
    },
    {
      title: 'Evénements privés',
      url: 'events',
      icon: 'calendar-outline'
    },
    {
      title: 'Mon compte',
      url: 'account',
      icon: 'person-circle-outline'
    },
    {
      title: 'Clavem gift',
      url: 'clavem-gift',
      icon: 'gift-outline'
    },
    {
      title: 'Noter l\'application',
      url: 'notes',
      icon: 'star'
    },
  ];
  selectedPath = '';


  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    })
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.pages.findIndex(
        (page) => page.title.toLowerCase() === path.toLowerCase()
      );
    }
  }
}

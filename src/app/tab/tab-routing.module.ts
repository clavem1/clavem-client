import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabPage } from './tab.page';

const routes: Routes = [
  {
    path: '',
    component: TabPage,
    children: [
      {
        path: 'message',
        loadChildren: () => import('./message/message.module').then(m => m.MessagePageModule)
      },
      {
        path: 'group',
        loadChildren: () => import('./group/group.module').then(m => m.GroupPageModule)
      },
      {
        path: 'news',
        loadChildren: () => import('./news/news.module').then(m => m.NewsPageModule)
      },
      {
        path: '',
        redirectTo: '/tab/news',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: 'discussion',
    loadChildren: () => import('./discussion/discussion.module').then( m => m.DiscussionPageModule)
  },
  {
    path: 'conversation',
    loadChildren: () => import('./conversation/conversation.module').then( m => m.ConversationPageModule)
  },
  {
    path: 'detail',
    loadChildren: () => import('./group/detail/detail.module').then(m => m.DetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabPageRoutingModule {}

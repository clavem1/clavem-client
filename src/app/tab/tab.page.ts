import { Component, OnInit } from '@angular/core';
import { MenuPage } from './menu/menu.page';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.page.html',
  styleUrls: ['./tab.page.scss'],
})
export class TabPage implements OnInit {

  constructor(private popoverCtrl: PopoverController) { }

  ngOnInit() {
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: MenuPage,
      event: ev,
      showBackdrop: false,
      cssClass: 'list'
    });
    return await popover.present();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClosingCardPage } from './closing-card.page';

describe('ClosingCardPage', () => {
  let component: ClosingCardPage;
  let fixture: ComponentFixture<ClosingCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosingCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClosingCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

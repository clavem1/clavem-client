import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClosingCardPage } from './closing-card.page';

const routes: Routes = [
  {
    path: '',
    component: ClosingCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClosingCardPageRoutingModule {}

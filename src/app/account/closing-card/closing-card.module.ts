import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClosingCardPageRoutingModule } from './closing-card-routing.module';

import { ClosingCardPage } from './closing-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClosingCardPageRoutingModule
  ],
  declarations: [ClosingCardPage]
})
export class ClosingCardPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LostCardPage } from './lost-card.page';

describe('LostCardPage', () => {
  let component: LostCardPage;
  let fixture: ComponentFixture<LostCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LostCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LostCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

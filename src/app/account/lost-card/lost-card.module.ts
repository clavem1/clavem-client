import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LostCardPageRoutingModule } from './lost-card-routing.module';

import { LostCardPage } from './lost-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LostCardPageRoutingModule
  ],
  declarations: [LostCardPage]
})
export class LostCardPageModule {}

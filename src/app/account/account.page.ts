import { Component, OnInit } from '@angular/core';
import { PopoverController, Platform } from '@ionic/angular';
import { OptionPage } from './option/option.page';
import { IUser } from '../shared/typings/interfaces/user.interface';
import { AuthService } from '../auth/service/auth.service';
import { FetchCurrentUserGQL, FetchMyReservationGQL } from 'src/generated/graphql';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  currentUser;
  basePosterAPI = `${environment.API_URL}/multimedia/images/avatar`;
  reservations = [];
  constructor(
    private popoverCtrl: PopoverController,
    private authService: AuthService,
    private readonly fetchCurrentUserGQL: FetchCurrentUserGQL,
    private readonly fetchMyReservations: FetchMyReservationGQL,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.fetchCurrentUserGQL.fetch({}).subscribe(
      (result) => {
        this.currentUser = result.data.fetchCurrentUser;
      },
      (error) => {
        alert('Error when fetch currentUser');
      }
    );
    this.fetchMyReservations.fetch({}).subscribe(
      (result) => {
        this.reservations = result.data.fetchMyReservations;
        console.log(this.reservations);

      }
    )
    this.activatedRoute.queryParams.subscribe(
      (params) => {
        const cardNumber = params.cardNumber || null;
        console.log('NAVIGATE BEFORE')
        if (cardNumber) {
          console.log('NAVIGATE')
          this.router.navigate(['/cards/associate'], { queryParams: { cardNumber } })
        }
      }
    )
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: OptionPage,
      event: ev,
      showBackdrop: false,
      cssClass: 'my-custom-class'
    });
    return await popover.present();
  }

  generateUrl(url) {
    return this.basePosterAPI + '/' + url;
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardsPageRoutingModule } from './cards-routing.module';

import { CardsPage } from './cards.page';
import { AccesPage } from './acces/acces.page';
import { ReservationPage } from './reservation/reservation.page';
import { HeaderPageModule } from '../shared/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderPageModule,
    CardsPageRoutingModule
  ],
  declarations: [CardsPage, AccesPage, ReservationPage],
  exports: [HeaderPageModule]
})
export class CardsPageModule { }

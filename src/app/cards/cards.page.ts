import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchMyCardsGQL } from 'src/generated/graphql';
import { Platform, IonRouterOutlet } from '@ionic/angular';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.page.html',
  styleUrls: ['./cards.page.scss'],
})
export class CardsPage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true
  };
  currentIndex = 0;
  reservation = false;
  acces = true;
  cards = [];


  constructor(
    private router: Router,
    private readonly fetchMyCardsGQL: FetchMyCardsGQL
  ) { }

  ngOnInit() {
    this.fetchMyCardsGQL.fetch({}).subscribe(
      (result) => {
        this.cards = result.data.fetchMyCards;
      }
    )
  }

  onNavigate() {
    this.router.navigate(['cards', 'payment']);
  }

  onSlide(slides) {
    slides.getActiveIndex().then((index) => {
      this.currentIndex = index;
    })

  }

  toggleAcces() {
    this.acces = true;
    this.reservation = false;
  }

  toggleReservation() {
    this.reservation = true;
    this.acces = false;
  }


}

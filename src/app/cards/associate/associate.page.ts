import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController, Platform, NavController } from '@ionic/angular';
import { CreateCardGQL } from 'src/generated/graphql';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Location } from '@angular/common';

@Component({
  selector: 'app-associate',
  templateUrl: './associate.page.html',
  styleUrls: ['./associate.page.scss'],
})
export class AssociatePage implements OnInit {
  cardNumber: string = "";
  showCamera: boolean;
  scanSub: any;
  constructor(
    private platform: Platform,
    private readonly createCardGQL: CreateCardGQL,
    public toastController: ToastController,
    public router: Router,
    private qrScanner: QRScanner,
    public alertController: AlertController,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    public navCtrl: NavController,
  ) {
    
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      (params) => {
        this.cardNumber = params.cardNumber || null;
      }
    )
  }

  createCard() {
    if (!this.cardNumber || String(this.cardNumber).trim() === "") {
      this.presentToast('danger', 'Numéro invalide!');
      return;
    }
    this.createCardGQL.mutate({ cardNumber: String(this.cardNumber) }).subscribe(
      (result) => {
        if (!result.errors) {
          this.presentToast('success', 'Carte ajoutée avec succés!').finally(() => this.router.navigate(['/cards']));
          this.cardNumber = "";
        } else {
          this.presentToast('danger', 'Erreur!');
        }
      },
      (error) => {
        this.presentToast('danger', 'Erreur!');
      }
    )
  }

  scancode() {
    this.platform.backButton.subscribeWithPriority(0, () => {
      this.showCamera = false;
      this.qrScanner.hide(); // hide camera preview
      this.qrScanner.destroy();
    });
    this.showCamera = true;
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted

          // start scanning
          this.scanSub = this.qrScanner.scan().subscribe((text: any) => {
            this.cardNumber = text;
            this.showCamera = false;
            this.qrScanner.hide(); // hide camera preview
            this.qrScanner.destroy();
            this.scanSub.unsubscribe();
            this.router.navigate(['cards'], {queryParams: { cardNumber: text }});
          });
        } else if (status.denied) {
          console.log('DENIED ')
        } else {
          console.log('DENIED NOT PERMANENT ')
        }
      })
      .catch((e: any) => console.log('Error is', e))
      ;
  }

  async presentToast(color, message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: color
    });
    toast.present();
  }

}

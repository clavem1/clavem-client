import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AssociatePageRoutingModule } from './associate-routing.module';

import { AssociatePage } from './associate.page';
import { QRCodeModule } from 'angularx-qrcode';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AssociatePageRoutingModule,
    QRCodeModule
  ],
  declarations: [AssociatePage],
  providers: [
    QRScanner,
  ]
})
export class AssociatePageModule {}

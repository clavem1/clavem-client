import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssociatePage } from './associate.page';

const routes: Routes = [
  {
    path: '',
    component: AssociatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssociatePageRoutingModule {}

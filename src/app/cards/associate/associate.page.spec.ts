import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AssociatePage } from './associate.page';

describe('AssociatePage', () => {
  let component: AssociatePage;
  let fixture: ComponentFixture<AssociatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssociatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AssociatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

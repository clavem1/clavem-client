import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-transfer-acces',
  templateUrl: './transfer-acces.page.html',
  styleUrls: ['./transfer-acces.page.scss'],
})
export class TransferAccesPage implements OnInit {

  cardNumber: string;
  eventId: string;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.eventId = this.activatedRoute.snapshot.paramMap.get('eventId');
  }

  createCard() { }

}

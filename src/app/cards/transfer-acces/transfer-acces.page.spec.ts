import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TransferAccesPage } from './transfer-acces.page';

describe('TransferAccesPage', () => {
  let component: TransferAccesPage;
  let fixture: ComponentFixture<TransferAccesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferAccesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TransferAccesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

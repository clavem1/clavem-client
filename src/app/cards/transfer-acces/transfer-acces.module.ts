import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransferAccesPageRoutingModule } from './transfer-acces-routing.module';

import { TransferAccesPage } from './transfer-acces.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransferAccesPageRoutingModule
  ],
  declarations: [TransferAccesPage]
})
export class TransferAccesPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransferAccesPage } from './transfer-acces.page';

const routes: Routes = [
  {
    path: '',
    component: TransferAccesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransferAccesPageRoutingModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardsPage } from './cards.page';

const routes: Routes = [
  {
    path: '',
    component: CardsPage
  },
  {
    path: 'lost-card',
    loadChildren: () => import('./lost-card/lost-card.module').then(m => m.LostCardPageModule)
  },
  {
    path: 'order-card',
    loadChildren: () => import('./order-card/order-card.module').then(m => m.OrderCardPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./payment/payment.module').then(m => m.PaymentPageModule)
  },
  {
    path: 'associate',
    loadChildren: () => import('./associate/associate.module').then(m => m.AssociatePageModule)
  },
  {
    path: 'generate-qr-code',
    loadChildren: () => import('./generate-qr-code/generate-qr-code.module').then(m => m.GenerateQrCodePageModule)
  },
  {
    path: 'view-qr-code',
    loadChildren: () => import('./view-qr-code/view-qr-code.module').then(m => m.ViewQrCodePageModule)
  },
  {
    path: 'confirm-qr-code',
    loadChildren: () => import('./confirm-qr-code/confirm-qr-code.module').then(m => m.ConfirmQrCodePageModule)
  },
  {
    path: 'transfer-acces/:eventId',
    loadChildren: () => import('./transfer-acces/transfer-acces.module').then(m => m.TransferAccesPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardsPageRoutingModule { }

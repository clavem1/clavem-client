import { Component, OnInit } from '@angular/core';
import { QRScannerStatus, QRScanner } from '@ionic-native/qr-scanner/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-generate-qr-code',
  templateUrl: './generate-qr-code.page.html',
  styleUrls: ['./generate-qr-code.page.scss'],
})
export class GenerateQrCodePage implements OnInit {

  showCamera: boolean;
  scanSub: any;
  constructor(private platform: Platform, private qrScanner: QRScanner) {
    this.platform.backButton.subscribeWithPriority(0, () => {
      document.getElementsByTagName('body')[0].style.opacity = "1";
      this.showCamera = false;
      this.qrScanner.hide(); // hide camera preview
      this.qrScanner.destroy();
    })
  }

  ngOnInit() {
    this.scancode()
  }

  scancode() {
    this.showCamera = true;
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          this.qrScanner.show();
          document.getElementsByTagName('body')[0].style.opacity = "0";

          // start scanning
          this.scanSub = this.qrScanner.scan().subscribe((text: any) => {
            console.log('Scanned something', text.result);
            document.getElementsByTagName('body')[0].style.opacity = "1";

            this.showCamera = false;
            this.qrScanner.hide(); // hide camera preview
            this.scanSub.unsubscribe(); // stop scanning
          });

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => console.log('Error is', e));
  }


}

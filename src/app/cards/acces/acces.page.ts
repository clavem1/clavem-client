import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { FetchMyReservationGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-acces',
  templateUrl: './acces.page.html',
  styleUrls: ['./acces.page.scss'],
})
export class AccesPage implements OnInit {

  reservations = [];
  basePosterAPI = `${environment.API_URL}/event/poster`;
  constructor(
    private readonly fetchMyReservations: FetchMyReservationGQL,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.fetchMyReservations.fetch({}).subscribe(
      (result) => {
        this.reservations = result.data.fetchMyReservations;
      }
    )
    this.activatedRoute.queryParams.subscribe(
      (params) => {
        const cardNumber = params.cardNumber || null;
        console.log('NAVIGATE BEFORE')
        if (cardNumber) {
          console.log('NAVIGATE')
          this.router.navigate(['/cards/associate'], { queryParams: { cardNumber } })
        }
      }
    )
  }

  generateUrl(url) {
    return this.basePosterAPI + '/' + url;
  }
}

import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FetchMyReservationGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.page.html',
  styleUrls: ['./reservation.page.scss'],
})
export class ReservationPage implements OnInit {

  reservations = [];
  basePosterAPI = `${environment.API_URL}/event/poster`;
  constructor(
    private readonly fetchMyReservations: FetchMyReservationGQL
  ) { }

  ngOnInit() {
    this.fetchMyReservations.fetch({}).subscribe(
      (result) => {
        this.reservations = result.data.fetchMyReservations;
      }
    )
  }

  generateUrl(url) {
    return this.basePosterAPI + '/' + url;
  }

}

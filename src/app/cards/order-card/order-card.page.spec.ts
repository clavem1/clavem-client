import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderCardPage } from './order-card.page';

describe('OrderCardPage', () => {
  let component: OrderCardPage;
  let fixture: ComponentFixture<OrderCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

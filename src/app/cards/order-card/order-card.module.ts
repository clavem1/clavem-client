import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderCardPageRoutingModule } from './order-card-routing.module';

import { OrderCardPage } from './order-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderCardPageRoutingModule
  ],
  declarations: [OrderCardPage]
})
export class OrderCardPageModule {}

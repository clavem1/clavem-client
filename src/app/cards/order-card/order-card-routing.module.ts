import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderCardPage } from './order-card.page';

const routes: Routes = [
  {
    path: '',
    component: OrderCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderCardPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/shared/services/toast.service';
import { FetchMyCardsGQL, LostCardGQL } from 'src/generated/graphql';
import { QRScannerStatus, QRScanner } from '@ionic-native/qr-scanner/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-lost-card',
  templateUrl: './lost-card.page.html',
  styleUrls: ['./lost-card.page.scss'],
})
export class LostCardPage implements OnInit {
  cards = [];
  cardNumber: number;
  emergencyCard: number;
  transfertCard: Boolean = true;
  showCamera: boolean = false;
  scanSub: any;
  constructor(
    private platform: Platform,
    private readonly fetchMyCardsGQL: FetchMyCardsGQL,
    private toastService: ToastService,
    private readonly lostCardGQL: LostCardGQL,
    private readonly router: Router,
    private qrScanner: QRScanner
  ) {
    this.platform.backButton.subscribeWithPriority(0, () => {
      this.qrScanner.hide(); // hide camera preview
      this.scanSub.unsubscribe(); // stop scanning
      this.showCamera = false;
    })
  }

  ngOnInit() {
    this.fetchMyCardsGQL.fetch({}).subscribe(
      (result) => {
        if (!result.errors) {
          this.cards = result.data.fetchMyCards;
        } else {
          this.toastService.presentToast('danger', 'Erreur lors de la récupérations de vos cartes!');
        }
      }
    )
  }

  lostCard() {
    if (!this.lostCard || String(this.lostCard).trim() === '') {
      this.toastService.presentToast('danger', 'Carte invalide!');
      return;
    }
    const lostCardPayload: any = { cardNumber: String(this.cardNumber) };
    if (this.transfertCard && this.emergencyCard && String(this.emergencyCard) !== '') {
      lostCardPayload.emergencyCard = String(this.emergencyCard);
    }
    this.lostCardGQL.mutate(lostCardPayload).subscribe(
      (result) => {
        if (!result.errors) {
          this.toastService.presentToast('success', 'Votre carte a été déclaré perdu!').finally(() => this.router.navigate(['/cards']));
        } else {
          this.toastService.presentToast('danger', 'Erreur lors de la déclaration!');
        }
      },
      (error) => {
        this.toastService.presentToast('danger', 'Erreur lors de la déclaration!');
      }
    )
  }

  scancode() {
    this.showCamera = true;
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          this.qrScanner.show();

          // start scanning
          this.scanSub = this.qrScanner.scan().subscribe((text: any) => {
            console.log('Scanned something', text.result);
            this.cardNumber = Number(text.result);

            this.showCamera = false;
            this.qrScanner.hide(); // hide camera preview
            this.scanSub.unsubscribe(); // stop scanning
          });

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => console.log('Error is', e));
  }
}

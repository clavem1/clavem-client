import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LostCardPage } from './lost-card.page';

const routes: Routes = [
  {
    path: '',
    component: LostCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LostCardPageRoutingModule {}

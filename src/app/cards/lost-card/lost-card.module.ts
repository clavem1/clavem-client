import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LostCardPageRoutingModule } from './lost-card-routing.module';

import { LostCardPage } from './lost-card.page';
import { ToastService } from 'src/app/shared/services/toast.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LostCardPageRoutingModule
  ],
  declarations: [LostCardPage],
  providers: [ToastService]
})
export class LostCardPageModule {}

import { Component, OnInit } from '@angular/core';
import { FetchMyCardsGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-card',
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss'],
})
export class CardPage implements OnInit {
  cards = [];
  constructor(
    private readonly fetchMyCardsGQL: FetchMyCardsGQL,
  ) { }

  ngOnInit() {
    this.fetchMyCardsGQL.fetch({}).subscribe(
      (result) => {
        this.cards = result.data.fetchMyCards;
      }
    )
  }

}

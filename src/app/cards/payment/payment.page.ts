import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  visa = true;
  orangeMoney = false;
  expresso = false;
  freeMoney = false;

  constructor() { }

  ngOnInit() {
  }

  visaSelected() {
    this.visa = true;
    this.orangeMoney = this.expresso = this.freeMoney = false;
  }

  orangeSelected() {
    this.orangeMoney = true;
    this.visa = this.expresso = this.freeMoney = false;
  }

  freeSelected() {
    this.freeMoney = true;
    this.orangeMoney = this.visa = this.expresso = false;
  }

  expressoSelected() {
    this.expresso = true;
    this.orangeMoney = this.visa = this.freeMoney = false;
  }
}

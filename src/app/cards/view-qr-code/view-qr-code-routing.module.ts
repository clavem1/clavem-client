import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewQrCodePage } from './view-qr-code.page';

const routes: Routes = [
  {
    path: '',
    component: ViewQrCodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewQrCodePageRoutingModule {}

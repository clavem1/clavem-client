import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewQrCodePage } from './view-qr-code.page';

describe('ViewQrCodePage', () => {
  let component: ViewQrCodePage;
  let fixture: ComponentFixture<ViewQrCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewQrCodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewQrCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewQrCodePageRoutingModule } from './view-qr-code-routing.module';

import { ViewQrCodePage } from './view-qr-code.page';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QRCodeModule,
    ViewQrCodePageRoutingModule
  ],
  declarations: [ViewQrCodePage]
})
export class ViewQrCodePageModule { }

import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { UserRole } from 'src/app/shared/typings/enums/user-role';
import { UserRoles } from 'src/generated/graphql';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-view-qr-code',
  templateUrl: './view-qr-code.page.html',
  styleUrls: ['./view-qr-code.page.scss'],
})
export class ViewQrCodePage implements OnInit {

  textQRCode: string = null;
  constructor(private user: AuthService) { }

  ngOnInit() {
    const user = this.user.getCurrentUser()
    this.textQRCode = user['_id'] + new Date().getTime().toString();
  }
}

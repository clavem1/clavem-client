import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmQrCodePage } from './confirm-qr-code.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmQrCodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmQrCodePageRoutingModule {}

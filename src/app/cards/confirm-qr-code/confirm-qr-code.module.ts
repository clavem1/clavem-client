import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmQrCodePageRoutingModule } from './confirm-qr-code-routing.module';

import { ConfirmQrCodePage } from './confirm-qr-code.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmQrCodePageRoutingModule
  ],
  declarations: [ConfirmQrCodePage]
})
export class ConfirmQrCodePageModule {}

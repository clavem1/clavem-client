import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Platform, IonRouterOutlet } from '@ionic/angular';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

  constructor(private router: Router, private socialSharing: SocialSharing,
    private platform: Platform, private routerOutlet: IonRouterOutlet) {
    this.platform.backButton.subscribeWithPriority(0, () => {
      if (!this.routerOutlet.canGoBack()) {
        this.router.navigate(['home']);
      }
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.router.navigate(['events', 'private-event']);
  }
  shareYourCurrentApp() {
    let appUrl = "https://play.google.coom/store/apps/details?id=io.ionic.starter";
    this.socialSharing.share(null, null, null, appUrl).then(() => {

    }).catch((error) => {
      console.log(error);
    })
  }
}

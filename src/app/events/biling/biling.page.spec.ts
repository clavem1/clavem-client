import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BilingPage } from './biling.page';

describe('BilingPage', () => {
  let component: BilingPage;
  let fixture: ComponentFixture<BilingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BilingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

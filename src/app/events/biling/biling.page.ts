import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastService } from 'src/app/shared/services/toast.service';
import { CreateReservationGQL, FetchCurrentUserGQL, FetchEventGQL } from 'src/generated/graphql';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-biling',
  templateUrl: './biling.page.html',
  styleUrls: ['./biling.page.scss'],
})
export class BilingPage implements OnInit {
  currentUser;
  cardNumber = null;
  ticketId = null;
  ticketForOther = null;
  cardForOther = null;
  event;
  constructor(
    private readonly fetchCurrentUser: FetchCurrentUserGQL,
    private readonly fetchEventGQL: FetchEventGQL,
    private readonly createReservationGQL: CreateReservationGQL,
    private readonly activatedRoute: ActivatedRoute,
    private readonly toastService: ToastService,
    private readonly router: Router,
    private socialSharing: SocialSharing
  ) { }

  ngOnInit() {
    const eventId = this.activatedRoute.snapshot.paramMap.get('eventId');
    this.fetchEventGQL.fetch({ eventId }).subscribe(
      (result) => {
        this.event = result.data.fetchEvent;
      }
    )
    this.fetchCurrentUser.fetch({}).subscribe(
      (result) => {
        this.currentUser = result.data.fetchCurrentUser;
      }
    )
  }

  ticket(ticketId) {
    if (!this.event || !this.event.tickets) {
      return null;
    }
    let choosedTicket = null;
    this.event.tickets.map((t) => {
      if (t._id === ticketId) {
        choosedTicket = t;
      }
    });
    return choosedTicket;
  }

  totalPrice() {
    const ticketForMe = this.ticket(this.ticketId);
    const ticketForOther = this.ticket(this.ticketForOther);
    return (ticketForMe ? ticketForMe.price : 0) + (ticketForOther ? ticketForOther.price : 0);
  }

  reserve() {
    this.createReservationGQL.
      mutate({ reservationInput: { card: this.cardNumber, cardForOther: this.cardForOther, ticket: this.ticketId, ticketForOther: this.ticketForOther } })
      .subscribe(
        (result) => {
          if (!result.errors) {
            this.toastService.presentToast('success', 'Réservation succés!');
            this.router.navigate(['/events/private-event'])
          } else {
            this.toastService.presentToast('danger', 'Erreur: Vérifier si la carte ajoutée est bonne et la réservation pas déjà faite');
          }
        },
        (error) => {
          this.toastService.presentToast('danger', 'Erreur: Vérifier si la carte ajoutée est bonne et la réservation pas déjà faite');
        }
      )
  }

  shareYourCurrentApp() {
    let appUrl = "https://play.google.coom/store/apps/details?id=io.ionic.starter";
    this.socialSharing.share(null, null, null, appUrl).then(() => {

    }).catch((error) => {
      console.log(error);
    })
  }
}

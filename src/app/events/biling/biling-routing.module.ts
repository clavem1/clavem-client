import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BilingPage } from './biling.page';

const routes: Routes = [
  {
    path: '',
    component: BilingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BilingPageRoutingModule {}

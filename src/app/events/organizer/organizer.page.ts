import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.page.html',
  styleUrls: ['./organizer.page.scss'],
})
export class OrganizerPage implements OnInit {

  constructor(private socialSharing: SocialSharing) { }

  ngOnInit() {
  }
  shareYourCurrentApp() {
    let appUrl = "https://play.google.coom/store/apps/details?id=io.ionic.starter";
    this.socialSharing.share(null, null, null, appUrl).then(() => {

    }).catch((error) => {
      console.log(error);
    })
  }

}

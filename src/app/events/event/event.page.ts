import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActivatedRoute } from '@angular/router';
import { FetchEventGQL } from 'src/generated/graphql';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-event',
  templateUrl: './event.page.html',
  styleUrls: ['./event.page.scss'],
})
export class EventPage implements OnInit {

  eventId: string;
  event
  basePosterAPI = `${environment.API_URL}/event/poster`;

  constructor(
    private socialSharing: SocialSharing,
    private activatedRoute: ActivatedRoute,
    private readonly fetchEventGQL: FetchEventGQL
  ) { }

  ngOnInit() {
    this.eventId = this.activatedRoute.snapshot.paramMap.get('eventId');
    this.fetchEventGQL.fetch({ eventId: this.eventId }).subscribe((result) => {
      this.event = result.data.fetchEvent;
      console.log(this.event);

    });
  }

  generateUrl(url) {
    return this.basePosterAPI + '/' + url;
  }
  shareYourCurrentApp() {
    let appUrl = "https://play.google.coom/store/apps/details?id=io.ionic.starter";
    this.socialSharing.share(null, null, null, appUrl).then(() => {

    }).catch((error) => {
      console.log(error);
    })
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrivateEventPage } from './private-event.page';

describe('PrivateEventPage', () => {
  let component: PrivateEventPage;
  let fixture: ComponentFixture<PrivateEventPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateEventPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrivateEventPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrivateEventPageRoutingModule } from './private-event-routing.module';

import { PrivateEventPage } from './private-event.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrivateEventPageRoutingModule
  ],
  declarations: [PrivateEventPage]
})
export class PrivateEventPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrivateEventPage } from './private-event.page';

const routes: Routes = [
  {
    path: '',
    component: PrivateEventPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivateEventPageRoutingModule {}

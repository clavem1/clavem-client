import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FetchMyReservationGQL } from 'src/generated/graphql';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-private-event',
  templateUrl: './private-event.page.html',
  styleUrls: ['./private-event.page.scss'],
})
export class PrivateEventPage implements OnInit {
  reservations = [];
  basePosterAPI = `${environment.API_URL}/event/poster`;
  constructor(
    private readonly fetchMyReservations: FetchMyReservationGQL,
    private socialSharing: SocialSharing
  ) { }

  ngOnInit() {
    this.fetchMyReservations.fetch({}).subscribe(
      (result) => {
        this.reservations = result.data.fetchMyReservations;
      }
    )
  }

  generateUrl(url) {
    return this.basePosterAPI + '/' + url;
  }

  shareYourCurrentApp() {
    let appUrl = "https://play.google.coom/store/apps/details?id=io.ionic.starter";
    this.socialSharing.share(null, null, null, appUrl).then(() => {

    }).catch((error) => {
      console.log(error);
    })
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventListPage } from './event-list.page';

const routes: Routes = [
  {
    path: '',
    component: EventListPage
  },
  {
    path: 'popover',
    loadChildren: () => import('./popover/popover.module').then( m => m.PopoverPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventListPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { PopoverPage } from './popover/popover.page';
import { PopoverController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { FetchCategoryEventsGQL } from 'src/generated/graphql';
import { environment } from 'src/environments/environment';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.page.html',
  styleUrls: ['./event-list.page.scss'],
})
export class EventListPage implements OnInit {
  categoryId: string;
  events = [];
  basePosterAPI = `${environment.API_URL}/event/poster`;

  constructor(
    private popoverCtrl: PopoverController,
    private readonly activatedRoute: ActivatedRoute,
    private readonly fetchCategoryEventsGQL: FetchCategoryEventsGQL,
    private socialSharing: SocialSharing
  ) { }

  ngOnInit() {
    this.categoryId = this.activatedRoute.snapshot.paramMap.get('categoryId');
    this.fetchCategoryEventsGQL.fetch({ categoryId: this.categoryId }).subscribe(
      (result) => {
        this.events = result.data.fetchCategoryEvents.records;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  async presentPopover(event: any) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      showBackdrop: false,
      cssClass: 'read-description',
      componentProps: { event }
    });
    return await popover.present();
  }

  generateUrl(url) {
    return this.basePosterAPI + '/' + url;
  }

  shareYourCurrentApp() {
    let appUrl = "https://play.google.coom/store/apps/details?id=io.ionic.starter";
    this.socialSharing.share(null, null, null, appUrl).then(() => {

    }).catch((error) => {
      console.log(error);
    })
  }
}

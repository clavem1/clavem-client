import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {
  event;
  constructor(private readonly navParams: NavParams) { }

  ngOnInit() {
  this.event = this.navParams.get('event');
  }

}

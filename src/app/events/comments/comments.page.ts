import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { environment } from 'src/environments/environment';
import { CreateCommentGQL, FetchClientCommentsGQL, FetchEventGQL } from 'src/generated/graphql';

@Component({
  selector: "app-comments",
  templateUrl: "./comments.page.html",
  styleUrls: ["./comments.page.scss"],
})
export class CommentsPage implements OnInit {
  comments = [];
  eventId;
  event;
  basePosterAPI = `${environment.API_URL}/event/poster`;
  newComment = "";
  constructor(
    private socialSharing: SocialSharing,
    private fetchClientCommentsGQL: FetchClientCommentsGQL,
    private createCommentGQL: CreateCommentGQL,
    private readonly activatedRoute: ActivatedRoute,
    private readonly fetchEventGQL: FetchEventGQL
  ) {}

  ngOnInit() {
    this.eventId = this.activatedRoute.snapshot.paramMap.get("eventId");
    this.fetchClientCommentsGQL.fetch({}).subscribe((result) => {
      this.comments = result.data.fetchClientComments;
    });
    this.fetchClientComments();
  }

  generateUrl(url) {
    return this.basePosterAPI + "/" + url;
  }

  shareYourCurrentApp() {
    let appUrl =
      "https://play.google.coom/store/apps/details?id=io.ionic.starter";
    this.socialSharing
      .share(null, null, null, appUrl)
      .then(() => {})
      .catch((error) => {
        console.log(error);
      });
  }

  fetchClientComments() {
    this.fetchEventGQL.fetch({ eventId: this.eventId }).subscribe((result) => {
      this.event = result.data.fetchEvent;
    });
  }

  postComment() {
    console.log({ message: this.newComment })
    this.createCommentGQL.mutate({commentDto: { destinataire: this.event.createdBy._id, message: this.newComment, event: this.eventId }}).subscribe(
      (result) => {
        this.newComment = "";
        this.comments.unshift(result.data.createComment)
        this.fetchClientComments();
      }
    )
  }
}

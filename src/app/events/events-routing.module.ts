import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventsPage } from './events.page';

const routes: Routes = [
  {
    path: '',
    component: EventsPage
  },
  {
    path: 'event/:eventId',
    loadChildren: () => import('./event/event.module').then(m => m.EventPageModule)
  },
  {
    path: 'private-event',
    loadChildren: () => import('./private-event/private-event.module').then(m => m.PrivateEventPageModule)
  },
  {
    path: ':eventId/comments',
    loadChildren: () => import('./comments/comments.module').then(m => m.CommentsPageModule)
  },
  {
    path: 'organizer',
    loadChildren: () => import('./organizer/organizer.module').then(m => m.OrganizerPageModule)
  },
  {
    path: 'biling/:eventId',
    loadChildren: () => import('./biling/biling.module').then(m => m.BilingPageModule)
  },
  {
    path: ':categoryId',
    loadChildren: () => import('./event-list/event-list.module').then(m => m.EventListPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventsPageRoutingModule { }

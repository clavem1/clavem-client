import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClavemGiftPageRoutingModule } from './clavem-gift-routing.module';

import { ClavemGiftPage } from './clavem-gift.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClavemGiftPageRoutingModule
  ],
  declarations: [ClavemGiftPage]
})
export class ClavemGiftPageModule {}

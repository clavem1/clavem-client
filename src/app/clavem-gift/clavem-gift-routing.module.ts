import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClavemGiftPage } from './clavem-gift.page';

const routes: Routes = [
  {
    path: '',
    component: ClavemGiftPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClavemGiftPageRoutingModule {}

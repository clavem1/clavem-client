import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClavemGiftPage } from './clavem-gift.page';

describe('ClavemGiftPage', () => {
  let component: ClavemGiftPage;
  let fixture: ComponentFixture<ClavemGiftPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClavemGiftPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClavemGiftPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

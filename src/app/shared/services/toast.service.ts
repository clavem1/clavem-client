import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable()
export class ToastService {
    constructor(public toastController: ToastController) {}

    async presentToast(color, message) {
        const toast = await this.toastController.create({
            message: message,
            duration: 2000,
            color: color
        });
        toast.present();
    }
}
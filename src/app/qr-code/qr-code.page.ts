import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController, Platform, NavController } from '@ionic/angular';
import { CreateCardGQL } from 'src/generated/graphql';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Location } from '@angular/common';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.page.html',
  styleUrls: ['./qr-code.page.scss'],
})
export class QrCodePage implements OnInit {
  constructor(private router: Router, public navCtrl: NavController, private qrScanner: QRScanner,
    public platform: Platform, private location: Location) {
    // solve the problem - "plugin not installed".
    platform.ready().then(() => {
      this.scan();
    });
  }

  ngOnInit() {
  }

  scan() {
    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          console.log('qrscaner authorized');
          // camera permission was granted
          // start scanning
          const scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);
            // alert(text);
            this.location.back(); // go to previous page
            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
          });

          this.qrScanner.resumePreview();

          // show camera preview
          this.qrScanner.show().then((data: QRScannerStatus) => {
            console.log('scaner show', data.showing);
          }, err => {
            alert(err);
          });
          // wait for user to scan something, then the observable callback will be called
        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
          if (!status.authorized && status.canOpenSettings) {
            if (confirm('Would you like to enable QR code scanning? You can allow camera access in your settings.')) {
              this.qrScanner.openSettings();
            }
          }
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => console.log('Error is', e));
  }

}

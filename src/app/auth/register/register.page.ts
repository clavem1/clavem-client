import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { AlertController, LoadingController } from '@ionic/angular';
import { RegisterGQL } from 'src/generated/graphql';
import { AuthService } from '../service/auth.service';
import { ToastService } from 'src/app/shared/services/toast.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  errorMessage: string;
  chipsVisibled = true;
  imageVisibled = false;
  acceptCondition = false;
  form: FormGroup;
  confirmPassword: string = "";

  constructor(
    private router: Router,
    private alertController: AlertController,
    private readonly registerGQL: RegisterGQL,
    private readonly authService: AuthService,
    private readonly toastService: ToastService,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    setInterval(() => {
      this.chipsVisibled = !this.chipsVisibled;
      this.imageVisibled = !this.imageVisibled
    }, 3000)

    this.form = new FormGroup({
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl(""),
      gender: new FormControl("", Validators.required),
      birthDate: new FormControl("", Validators.required),
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", Validators.required),
      confirmPassword: new FormControl("", Validators.required),
      phoneNumber: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      district: new FormControl("", Validators.required),
      conditionAccepted: new FormControl("", Validators.required),

    });
    this.form.setValue({
      "email": "genieouzboss@gmail.com",
      "password": "12345",
      "confirmPassword": "",
      "birthDate": "12/08/1993",
      "phoneNumber": "785238928",
      "firstName": "Ousmane",
      "lastName": "og Sakho",
      "gender": "M",
      "city": "Dakar",
      "district": "Damel",
      "conditionAccepted": false
    });
  }

  onSubmit() {
    if (this.form.value['password'] === this.form.value['confirmPassword']) {
      delete this.form.value['confirmPassword'];
      delete this.form.value['conditionAccepted'];
      this.presentAlertConfirm()
    } else {
      this.toastService.presentToast('danger', 'Les mots de passe ne sont pas identiques!');
    }
  }

  onConfirm(confirmPassword: any) {
    console.log(confirmPassword);

  }
  onAcceptCondition() {
    this.acceptCondition = !this.acceptCondition;
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmer !',
      message: 'Le genre et la date de naissance ne seront pas modifiés !!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log(this.form.value)
            this.loadingCtrl.create({ message: "chargement..." }).then(loading => {
              loading.present();
              const ref = this;

              this.registerGQL.mutate({ registerInput: this.form.value }).subscribe(
                (result) => {
                  if (result.errors) {
                    this.toastService.presentToast('danger', 'Vos identifiants ne sont pas correctes!');

                  }
                  const data = result.data.register;
                  this.authService.registerToken(data.token);
                  this.authService.registerCurrentUser(data.user);
                  this.authService.registerCurrentSession(data);
                  ref.loadingCtrl.dismiss();
                  this.router.navigate(['home']);
                },
                (error) => {
                  ref.loadingCtrl.dismiss();
                  this.toastService.presentToast('danger', 'Vos identifiants ne sont pas correctes!');
                }
              )
            })
          }
        }
      ]
    });

    await alert.present();
  }
}

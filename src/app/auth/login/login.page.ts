import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { LoginGQL } from 'src/generated/graphql';
import { ToastService } from 'src/app/shared/services/toast.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  errorMessage: string;
  username = {};
  chipsVisibled = true;
  imageVisibled = false;
  rememberChecked = true;

  constructor(
    private router: Router,
    private readonly authService: AuthService,
    private readonly loginGQL: LoginGQL,
    private readonly toastService: ToastService,
    private nativeStorage: NativeStorage,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    setInterval(() => {
      this.chipsVisibled = !this.chipsVisibled;
      this.imageVisibled = !this.imageVisibled
    }, 3000)
    this.nativeStorage.getItem('remember').then(
      data => {
        if (data == "true") {
          this.router.navigate(['home']);
        }
      },
      error => console.error(error)
    );
  }

  onRememberChecked() {
    this.rememberChecked = !this.rememberChecked;
  }
  onSubmit(form) {
    if (this.rememberChecked) {
      this.nativeStorage.setItem('remember', "true").then(
        () => console.log('On se souvient de vous !'),
        error => console.error('Error storing item', error)
      );
    }
    this.loadingCtrl.create({
      message: "Chargement..."
    }).then((loading) => {
      loading.present();
      let ref = this;
      this.loginGQL.fetch({ loginInput: form }).subscribe(
        (result) => {
          if (result.errors) {
            this.toastService.presentToast('danger', 'Vos identifiants ne sont pas correctes!');
          } else {
            const data = result.data.login;
            this.authService.registerToken(data.token);
            this.authService.registerCurrentUser(data.user);
            this.authService.registerCurrentSession(data);
            ref.loadingCtrl.dismiss();
            this.router.navigate(['home']);
          }
        },
        (error) => {
          ref.loadingCtrl.dismiss();
          this.toastService.presentToast('danger', 'Vos identifiants ne sont pas correctes!');
        }
      )
    })
  }

  forgotPassword() {
    this.router.navigate(['start-reset-password']);
  }
}

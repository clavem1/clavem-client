import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  chipsVisibled = true;
  imageVisibled = false;

  constructor() { }

  ngOnInit() {
    setInterval(() => {
      this.chipsVisibled = !this.chipsVisibled;
      this.imageVisibled = !this.imageVisibled
    }, 3000)
  }

}

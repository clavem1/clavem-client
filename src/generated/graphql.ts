import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
  /** Arbitrary object */
  Any: any;
};



export type AccessCodeDto = {
  username: Scalars['String'];
  password: Scalars['String'];
  role: AccessCodeRole;
};

export type AccessCodeEntity = {
  __typename?: 'AccessCodeEntity';
  _id: Scalars['ID'];
  organizer: UserEntity;
  username: Scalars['String'];
  password: Scalars['String'];
  role: AccessCodeRole;
};

export enum AccessCodeRole {
  Admin = 'ADMIN',
  Editor = 'EDITOR',
  ParticipantsManager = 'PARTICIPANTS_MANAGER',
  Observer = 'OBSERVER'
}

export type AccessCodesEntity = {
  __typename?: 'AccessCodesEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<AccessCodeEntity>;
};


export type AttachmentRecord = {
  __typename?: 'AttachmentRecord';
  id: Scalars['ID'];
  sizeB: Scalars['Int'];
  extension: Scalars['String'];
};

export type CardEntity = {
  __typename?: 'CardEntity';
  _id: Scalars['ID'];
  number: Scalars['String'];
  owner: UserEntity;
};

export type CardsEntity = {
  __typename?: 'CardsEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<CardEntity>;
};

export type CategoriesEntity = {
  __typename?: 'CategoriesEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<CategoryEntity>;
};

export type CategoryEntity = {
  __typename?: 'CategoryEntity';
  _id: Scalars['ID'];
  name: Scalars['String'];
  refused: Scalars['Float'];
  validated: Scalars['Float'];
  blocked: Scalars['Float'];
  pending: Scalars['Float'];
  activated: Scalars['Float'];
  desactivated: Scalars['Float'];
  archived: Scalars['Float'];
  paidEntrance: Scalars['Float'];
  freeEntrance: Scalars['Float'];
};

export type CategoryInput = {
  name: Scalars['String'];
};

export type ClientFilterInput = {
  offset?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
  filter?: Maybe<Scalars['Any']>;
  search?: Maybe<Scalars['String']>;
  orderBy?: Maybe<OrderByInput>;
};

export type CommentDto = {
  message: Scalars['String'];
  destinataire: Scalars['ID'];
  event: Scalars['ID'];
};

export type CommentEntity = {
  __typename?: 'CommentEntity';
  id: Scalars['ID'];
  message: Scalars['String'];
  createdAt: Scalars['DateTime'];
  sender: UserEntity;
  destinataire: UserEntity;
  event: EventEntity;
};


export enum EventAccessType {
  Uniq = 'Uniq',
  Free = 'Free'
}

export type EventEntity = {
  __typename?: 'EventEntity';
  _id: Scalars['ID'];
  description: Scalars['String'];
  address: Scalars['String'];
  locationAccuracy: Scalars['String'];
  state: EventState;
  status: EventStatus;
  type: EventType;
  name: Scalars['String'];
  catchyPhrase: Scalars['String'];
  createdBy: UserEntity;
  category: CategoryEntity;
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
  expectedNumberOfPersons: Scalars['Float'];
  accessType: EventAccessType;
  keepContactWithParticipant: Scalars['Boolean'];
  paidEntrance: Scalars['Boolean'];
  priceIncludingCharges: Scalars['Boolean'];
  categoryCriteria: Array<Scalars['String']>;
  purchasedTicketInvolveFreeTicket: TicketRequirements;
  tickets: Array<Ticket>;
  reservation: ReservationRequirements;
  poster: ImageSizes;
};

export type EventsEntity = {
  __typename?: 'EventsEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<EventEntity>;
};

export enum EventState {
  Activated = 'ACTIVATED',
  Desactivated = 'DESACTIVATED',
  Archived = 'ARCHIVED'
}

export enum EventStatus {
  Validated = 'VALIDATED',
  Refused = 'REFUSED',
  Pending = 'PENDING',
  Blocked = 'BLOCKED'
}

export enum EventType {
  Private = 'Private',
  Public = 'Public'
}

export type ImageSizes = {
  __typename?: 'ImageSizes';
  sm?: Maybe<Scalars['ID']>;
  md?: Maybe<Scalars['ID']>;
  lg?: Maybe<Scalars['ID']>;
};

export enum InternalRole {
  Strategic = 'STRATEGIC',
  Finance = 'FINANCE',
  Marketing = 'MARKETING',
  Standard = 'STANDARD'
}

export type LoginDto = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createComment: CommentEntity;
  changeEventState: EventEntity;
  changeEventStatus: EventEntity;
  createTicket: EventEntity;
  removeTicket: EventEntity;
  createCard: CardEntity;
  lostCard: CardEntity;
  register: SessionEntity;
  updatePassword: Scalars['Boolean'];
  createCategory: CategoryEntity;
  createPromotionalCode: PromotionalCodeEntity;
  createAccessCode: AccessCodeEntity;
  createReservation: Scalars['Boolean'];
};


export type MutationCreateCommentArgs = {
  commentInput: CommentDto;
};


export type MutationChangeEventStateArgs = {
  state: EventState;
  eventId: Scalars['ID'];
};


export type MutationChangeEventStatusArgs = {
  status: EventStatus;
  eventId: Scalars['ID'];
};


export type MutationCreateTicketArgs = {
  ticketDto: TicketDto;
  eventId: Scalars['ID'];
};


export type MutationRemoveTicketArgs = {
  ticketId: Scalars['ID'];
  eventId: Scalars['ID'];
};


export type MutationCreateCardArgs = {
  cardNumber: Scalars['String'];
};


export type MutationLostCardArgs = {
  emergencyCard?: Maybe<Scalars['String']>;
  cardNumber: Scalars['String'];
};


export type MutationRegisterArgs = {
  registerInput: RegisterDto;
};


export type MutationUpdatePasswordArgs = {
  updatePasswordDto: UpdatePasswordDto;
};


export type MutationCreateCategoryArgs = {
  categoryInput: CategoryInput;
};


export type MutationCreatePromotionalCodeArgs = {
  promotionalCodeDto: PromotionalCodeDto;
};


export type MutationCreateAccessCodeArgs = {
  accessCodeDto: AccessCodeDto;
};


export type MutationCreateReservationArgs = {
  reservationInput: ReservationInput;
};

/** OrderBy direction */
export enum OrderByDirection {
  Asc = 'Asc',
  Desc = 'Desc'
}

export type OrderByInput = {
  property: Scalars['String'];
  direction: OrderByDirection;
};

export type Paging = {
  __typename?: 'Paging';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
};

export type PreferenceDto = {
  category: Scalars['ID'];
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
  city: Scalars['String'];
  region: Scalars['String'];
  price: Scalars['Float'];
};

export type PromotionalCodeDto = {
  eventId: Scalars['ID'];
  impact: PromotionalCodeImpact;
  privateName: Scalars['String'];
  numberOfGeneratedCodes: Scalars['Float'];
  tickets: Array<Scalars['ID']>;
  reductionEffect: ReductionEffectDto;
  publicName: Scalars['String'];
  usableNumberOfTimes: Scalars['Float'];
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
};

export type PromotionalCodeEntity = {
  __typename?: 'PromotionalCodeEntity';
  _id: Scalars['String'];
  eventId: Scalars['ID'];
  event: EventEntity;
  impact: PromotionalCodeImpact;
  privateName: Scalars['String'];
  numberOfGeneratedCodes: Scalars['Float'];
  tickets: Array<Ticket>;
  reductionEffect: ReductionEffectEntity;
  publicName: Scalars['String'];
  usableNumberOfTimes: Scalars['Float'];
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
};

export enum PromotionalCodeImpact {
  MaskTickets = 'MASK_TICKETS',
  ApplyReduction = 'APPLY_REDUCTION'
}

export type PromotionalCodesEntity = {
  __typename?: 'PromotionalCodesEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<PromotionalCodeEntity>;
};

export type PubEntity = {
  __typename?: 'PubEntity';
  _id: Scalars['ID'];
  title: Scalars['String'];
  object: Scalars['String'];
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
  isUp: Scalars['Boolean'];
  dependence: Scalars['String'];
  attachment: Scalars['ID'];
  order: Scalars['Float'];
  createdAt: Scalars['DateTime'];
};

export type Query = {
  __typename?: 'Query';
  fetchComments: Array<CommentEntity>;
  fetchClientComments: Array<CommentEntity>;
  fetchEvents: EventsEntity;
  fetchEvent: EventEntity;
  fetchRecentEvents: EventsEntity;
  fetchCategoryEvents: EventsEntity;
  fetchPreferences: Array<EventEntity>;
  fetchCurrentUser: UserEntity;
  fetchAdmins: UsersEntity;
  fetchClients: UsersEntity;
  fetchCards: CardsEntity;
  fetchMyCards: Array<CardEntity>;
  fetchPubs: Array<PubEntity>;
  fetchUpPubs: Array<PubEntity>;
  fetchNoUpPubs: Array<PubEntity>;
  fetchActivePubs: Array<PubEntity>;
  fetchNextPubs: Array<PubEntity>;
  fetchUpActivePubs: Array<PubEntity>;
  fetchNoUpActivePubs: Array<PubEntity>;
  fetchNextUpPubs: Array<PubEntity>;
  fetchNextNoUpPubs: Array<PubEntity>;
  login: SessionEntity;
  resetPassword: Scalars['String'];
  fetchCategories: CategoriesEntity;
  fetchPromotionalCodes: PromotionalCodesEntity;
  fetchAccessCodes: AccessCodesEntity;
  fetchMyReservations: Array<ReservationEntity>;
  fetchEventReservations: Array<ReservationEntity>;
};


export type QueryFetchEventArgs = {
  eventId: Scalars['ID'];
};


export type QueryFetchCategoryEventsArgs = {
  categoryId: Scalars['ID'];
};


export type QueryFetchPreferencesArgs = {
  preferenceInput: PreferenceDto;
};


export type QueryLoginArgs = {
  loginInput: LoginDto;
};


export type QueryResetPasswordArgs = {
  email: Scalars['String'];
};


export type QueryFetchEventReservationsArgs = {
  eventId: Scalars['ID'];
};

export type ReductionEffectDto = {
  inPercentage: Scalars['Boolean'];
  reduction: Scalars['Float'];
};

export type ReductionEffectEntity = {
  __typename?: 'ReductionEffectEntity';
  inPercentage: Scalars['Boolean'];
  reduction: Scalars['Float'];
};

export type RegisterDto = {
  phoneNumber: Scalars['String'];
  lastName: Scalars['String'];
  firstName: Scalars['String'];
  password: Scalars['String'];
  email: Scalars['String'];
  birthDate: Scalars['DateTime'];
  gender: Scalars['String'];
  city: Scalars['String'];
  district: Scalars['String'];
};

export type ReservationEntity = {
  __typename?: 'ReservationEntity';
  _id: Scalars['ID'];
  card: CardEntity;
  ticket: Ticket;
  client: UserEntity;
  state: ReservationState;
  event: EventEntity;
  createdAt: Scalars['DateTime'];
};

export type ReservationInput = {
  card?: Maybe<Scalars['String']>;
  ticket?: Maybe<Scalars['String']>;
  cardForOther?: Maybe<Scalars['String']>;
  ticketForOther?: Maybe<Scalars['String']>;
};

export type ReservationRequirements = {
  __typename?: 'ReservationRequirements';
  allowed: Scalars['Boolean'];
  payWhenReservation: Scalars['Boolean'];
  reservationFeeRefundable: Scalars['Boolean'];
  percentageToPay: Scalars['Float'];
  limiteDateConfirmation: Scalars['DateTime'];
};

export enum ReservationState {
  Accepted = 'ACCEPTED',
  Refused = 'REFUSED',
  Pending = 'PENDING'
}

export type SessionEntity = {
  __typename?: 'SessionEntity';
  token: Scalars['String'];
  user: UserEntity;
};

export type Ticket = {
  __typename?: 'Ticket';
  _id: Scalars['ID'];
  name: Scalars['String'];
  categoryCriteria: Scalars['String'];
  quantity: Scalars['Float'];
  price: Scalars['Float'];
};

export type TicketDto = {
  name: Scalars['String'];
  categoryCriteria: Scalars['String'];
  quantity: Scalars['Float'];
  price: Scalars['Float'];
};

export type TicketRequirements = {
  __typename?: 'TicketRequirements';
  purchasedTicketInvolveFreeTicket: Scalars['Boolean'];
  purchasedTickets: TicketRequirementsQuantity;
  offeredTickets: TicketRequirementsQuantity;
};

export type TicketRequirementsQuantity = {
  __typename?: 'TicketRequirementsQuantity';
  quantity: Scalars['Float'];
  categoryCriteria: Scalars['String'];
};

export type UpdatePasswordDto = {
  password: Scalars['String'];
  resetToken: Scalars['String'];
  resetCode: Scalars['Float'];
};

export type UserDto = {
  countryCode: Scalars['String'];
  role: UserRoles;
  gender: UserGender;
  birthDate: Scalars['DateTime'];
  phoneNumber: Scalars['String'];
  lastName: Scalars['String'];
  firstName: Scalars['String'];
  email: Scalars['String'];
  internalRole: InternalRole;
};

export type UserEntity = {
  __typename?: 'UserEntity';
  _id: Scalars['ID'];
  countryCode?: Maybe<Scalars['String']>;
  role: UserRoles;
  internalRole?: Maybe<InternalRole>;
  gender?: Maybe<UserGender>;
  birthDate?: Maybe<Scalars['DateTime']>;
  phoneNumber?: Maybe<Scalars['String']>;
  lastName: Scalars['String'];
  firstName: Scalars['String'];
  password: Scalars['String'];
  email: Scalars['String'];
  avatar?: Maybe<ImageSizes>;
  city?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  cards: Array<CardEntity>;
};

export enum UserGender {
  Male = 'MALE',
  Female = 'FEMALE'
}

export enum UserRoles {
  User = 'USER',
  Admin = 'ADMIN',
  Organizer = 'ORGANIZER'
}

export type UsersEntity = {
  __typename?: 'UsersEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<UserEntity>;
};

export type FetchCurrentUserQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchCurrentUserQuery = (
  { __typename?: 'Query' }
  & { fetchCurrentUser: (
    { __typename?: 'UserEntity' }
    & Pick<UserEntity, '_id' | 'firstName' | 'lastName' | 'email' | 'role'>
    & { avatar?: Maybe<(
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    )>, cards: Array<(
      { __typename?: 'CardEntity' }
      & Pick<CardEntity, '_id' | 'number'>
    )> }
  ) }
);

export type RegisterMutationVariables = Exact<{
  registerInput: RegisterDto;
}>;


export type RegisterMutation = (
  { __typename?: 'Mutation' }
  & { register: (
    { __typename?: 'SessionEntity' }
    & Pick<SessionEntity, 'token'>
    & { user: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName' | 'phoneNumber' | 'email'>
      & { avatar?: Maybe<(
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'md'>
      )> }
    ) }
  ) }
);

export type LoginQueryVariables = Exact<{
  loginInput: LoginDto;
}>;


export type LoginQuery = (
  { __typename?: 'Query' }
  & { login: (
    { __typename?: 'SessionEntity' }
    & Pick<SessionEntity, 'token'>
    & { user: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName' | 'phoneNumber' | 'email'>
      & { avatar?: Maybe<(
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'md'>
      )> }
    ) }
  ) }
);

export type UpdatePasswordMutationVariables = Exact<{
  updatePasswordDto: UpdatePasswordDto;
}>;


export type UpdatePasswordMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'updatePassword'>
);

export type ResetPasswordQueryVariables = Exact<{
  email: Scalars['String'];
}>;


export type ResetPasswordQuery = (
  { __typename?: 'Query' }
  & Pick<Query, 'resetPassword'>
);

export type CreateCardMutationVariables = Exact<{
  cardNumber: Scalars['String'];
}>;


export type CreateCardMutation = (
  { __typename?: 'Mutation' }
  & { createCard: (
    { __typename?: 'CardEntity' }
    & Pick<CardEntity, 'number'>
  ) }
);

export type FetchMyCardsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchMyCardsQuery = (
  { __typename?: 'Query' }
  & { fetchMyCards: Array<(
    { __typename?: 'CardEntity' }
    & Pick<CardEntity, '_id' | 'number'>
  )> }
);

export type LostCardMutationVariables = Exact<{
  cardNumber: Scalars['String'];
  emergencyCard?: Maybe<Scalars['String']>;
}>;


export type LostCardMutation = (
  { __typename?: 'Mutation' }
  & { lostCard: (
    { __typename?: 'CardEntity' }
    & Pick<CardEntity, 'number'>
  ) }
);

export type CreateReservationMutationVariables = Exact<{
  reservationInput: ReservationInput;
}>;


export type CreateReservationMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'createReservation'>
);

export type FetchMyReservationQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchMyReservationQuery = (
  { __typename?: 'Query' }
  & { fetchMyReservations: Array<(
    { __typename?: 'ReservationEntity' }
    & { event: (
      { __typename?: 'EventEntity' }
      & Pick<EventEntity, '_id' | 'name' | 'status' | 'state' | 'address' | 'locationAccuracy' | 'startDate' | 'endDate' | 'catchyPhrase' | 'description'>
      & { tickets: Array<(
        { __typename?: 'Ticket' }
        & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
      )>, poster: (
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
      ), createdBy: (
        { __typename?: 'UserEntity' }
        & Pick<UserEntity, 'firstName' | 'lastName'>
      ) }
    ), ticket: (
      { __typename?: 'Ticket' }
      & Pick<Ticket, 'name' | 'price'>
    ), card: (
      { __typename?: 'CardEntity' }
      & Pick<CardEntity, 'number'>
    ) }
  )> }
);

export type FetchClientCommentsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchClientCommentsQuery = (
  { __typename?: 'Query' }
  & { fetchClientComments: Array<(
    { __typename?: 'CommentEntity' }
    & Pick<CommentEntity, 'message' | 'createdAt'>
    & { sender: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName'>
    ), destinataire: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName'>
    ) }
  )> }
);

export type CreateCommentMutationVariables = Exact<{
  commentDto: CommentDto;
}>;


export type CreateCommentMutation = (
  { __typename?: 'Mutation' }
  & { createComment: (
    { __typename?: 'CommentEntity' }
    & Pick<CommentEntity, 'message' | 'createdAt'>
    & { sender: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName'>
    ), destinataire: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName'>
    ) }
  ) }
);

export type FetchCategoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchCategoriesQuery = (
  { __typename?: 'Query' }
  & { fetchCategories: (
    { __typename?: 'CategoriesEntity' }
    & Pick<CategoriesEntity, 'limit' | 'recordsLength' | 'totalRecords' | 'offset' | 'pages' | 'currentPage'>
    & { records: Array<(
      { __typename?: 'CategoryEntity' }
      & Pick<CategoryEntity, '_id' | 'name'>
    )> }
  ) }
);

export type CreateCategoryMutationVariables = Exact<{
  categoryInput: CategoryInput;
}>;


export type CreateCategoryMutation = (
  { __typename?: 'Mutation' }
  & { createCategory: (
    { __typename?: 'CategoryEntity' }
    & Pick<CategoryEntity, '_id' | 'name'>
  ) }
);

export type FetchEventsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchEventsQuery = (
  { __typename?: 'Query' }
  & { fetchEvents: (
    { __typename?: 'EventsEntity' }
    & Pick<EventsEntity, 'limit' | 'recordsLength' | 'totalRecords' | 'offset' | 'pages' | 'currentPage'>
    & { records: Array<(
      { __typename?: 'EventEntity' }
      & Pick<EventEntity, '_id' | 'name' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
      & { tickets: Array<(
        { __typename?: 'Ticket' }
        & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
      )>, poster: (
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'md' | 'lg'>
      ) }
    )> }
  ) }
);

export type FetchCategoryEventsQueryVariables = Exact<{
  categoryId: Scalars['ID'];
}>;


export type FetchCategoryEventsQuery = (
  { __typename?: 'Query' }
  & { fetchCategoryEvents: (
    { __typename?: 'EventsEntity' }
    & Pick<EventsEntity, 'limit' | 'recordsLength' | 'totalRecords' | 'offset' | 'pages' | 'currentPage'>
    & { records: Array<(
      { __typename?: 'EventEntity' }
      & Pick<EventEntity, '_id' | 'name' | 'locationAccuracy' | 'description' | 'startDate' | 'catchyPhrase'>
      & { tickets: Array<(
        { __typename?: 'Ticket' }
        & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
      )>, poster: (
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
      ) }
    )> }
  ) }
);

export type FetchRecentEventsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchRecentEventsQuery = (
  { __typename?: 'Query' }
  & { fetchRecentEvents: (
    { __typename?: 'EventsEntity' }
    & Pick<EventsEntity, 'limit' | 'recordsLength' | 'totalRecords' | 'offset' | 'pages' | 'currentPage'>
    & { records: Array<(
      { __typename?: 'EventEntity' }
      & Pick<EventEntity, '_id' | 'name' | 'locationAccuracy' | 'startDate' | 'catchyPhrase' | 'state' | 'status'>
      & { tickets: Array<(
        { __typename?: 'Ticket' }
        & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
      )>, poster: (
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
      ) }
    )> }
  ) }
);

export type ChangeEventStatusMutationVariables = Exact<{
  eventId: Scalars['ID'];
  status: EventStatus;
}>;


export type ChangeEventStatusMutation = (
  { __typename?: 'Mutation' }
  & { changeEventStatus: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'status' | 'state' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
    & { tickets: Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>, poster: (
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    ), createdBy: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, 'firstName' | 'lastName'>
    ) }
  ) }
);

export type ChangeEventStateMutationVariables = Exact<{
  eventId: Scalars['ID'];
  state: EventState;
}>;


export type ChangeEventStateMutation = (
  { __typename?: 'Mutation' }
  & { changeEventState: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'status' | 'state' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
    & { tickets: Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>, poster: (
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    ), createdBy: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, 'firstName' | 'lastName'>
    ) }
  ) }
);

export type FetchEventQueryVariables = Exact<{
  eventId: Scalars['ID'];
}>;


export type FetchEventQuery = (
  { __typename?: 'Query' }
  & { fetchEvent: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'status' | 'state' | 'address' | 'locationAccuracy' | 'startDate' | 'endDate' | 'catchyPhrase' | 'description'>
    & { tickets: Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>, poster: (
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    ), createdBy: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName'>
    ) }
  ) }
);

export type FetchUpActivePubsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchUpActivePubsQuery = (
  { __typename?: 'Query' }
  & { fetchUpActivePubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchNoUpActivePubsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchNoUpActivePubsQuery = (
  { __typename?: 'Query' }
  & { fetchNoUpActivePubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchNextNoUpPubsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchNextNoUpPubsQuery = (
  { __typename?: 'Query' }
  & { fetchNextNoUpPubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchActivePubsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchActivePubsQuery = (
  { __typename?: 'Query' }
  & { fetchActivePubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchNextPubsQueryVariables = Exact<{ [key: string]: never; }>;


export type FetchNextPubsQuery = (
  { __typename?: 'Query' }
  & { fetchNextPubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export const FetchCurrentUserDocument = gql`
    query FetchCurrentUser {
  fetchCurrentUser {
    _id
    firstName
    lastName
    email
    role
    avatar {
      sm
      lg
      md
    }
    cards {
      _id
      number
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchCurrentUserGQL extends Apollo.Query<FetchCurrentUserQuery, FetchCurrentUserQueryVariables> {
    document = FetchCurrentUserDocument;
    
  }
export const RegisterDocument = gql`
    mutation Register($registerInput: RegisterDto!) {
  register(registerInput: $registerInput) {
    token
    user {
      _id
      firstName
      lastName
      phoneNumber
      email
      avatar {
        md
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RegisterGQL extends Apollo.Mutation<RegisterMutation, RegisterMutationVariables> {
    document = RegisterDocument;
    
  }
export const LoginDocument = gql`
    query Login($loginInput: LoginDto!) {
  login(loginInput: $loginInput) {
    token
    user {
      _id
      firstName
      lastName
      phoneNumber
      email
      avatar {
        md
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LoginGQL extends Apollo.Query<LoginQuery, LoginQueryVariables> {
    document = LoginDocument;
    
  }
export const UpdatePasswordDocument = gql`
    mutation updatePassword($updatePasswordDto: UpdatePasswordDto!) {
  updatePassword(updatePasswordDto: $updatePasswordDto)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdatePasswordGQL extends Apollo.Mutation<UpdatePasswordMutation, UpdatePasswordMutationVariables> {
    document = UpdatePasswordDocument;
    
  }
export const ResetPasswordDocument = gql`
    query resetPassword($email: String!) {
  resetPassword(email: $email)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ResetPasswordGQL extends Apollo.Query<ResetPasswordQuery, ResetPasswordQueryVariables> {
    document = ResetPasswordDocument;
    
  }
export const CreateCardDocument = gql`
    mutation CreateCard($cardNumber: String!) {
  createCard(cardNumber: $cardNumber) {
    number
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateCardGQL extends Apollo.Mutation<CreateCardMutation, CreateCardMutationVariables> {
    document = CreateCardDocument;
    
  }
export const FetchMyCardsDocument = gql`
    query FetchMyCards {
  fetchMyCards {
    _id
    number
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchMyCardsGQL extends Apollo.Query<FetchMyCardsQuery, FetchMyCardsQueryVariables> {
    document = FetchMyCardsDocument;
    
  }
export const LostCardDocument = gql`
    mutation lostCard($cardNumber: String!, $emergencyCard: String) {
  lostCard(cardNumber: $cardNumber, emergencyCard: $emergencyCard) {
    number
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LostCardGQL extends Apollo.Mutation<LostCardMutation, LostCardMutationVariables> {
    document = LostCardDocument;
    
  }
export const CreateReservationDocument = gql`
    mutation createReservation($reservationInput: ReservationInput!) {
  createReservation(reservationInput: $reservationInput)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateReservationGQL extends Apollo.Mutation<CreateReservationMutation, CreateReservationMutationVariables> {
    document = CreateReservationDocument;
    
  }
export const FetchMyReservationDocument = gql`
    query FetchMyReservation {
  fetchMyReservations {
    event {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      status
      state
      address
      locationAccuracy
      startDate
      endDate
      catchyPhrase
      description
      catchyPhrase
      createdBy {
        firstName
        lastName
      }
    }
    ticket {
      name
      price
    }
    card {
      number
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchMyReservationGQL extends Apollo.Query<FetchMyReservationQuery, FetchMyReservationQueryVariables> {
    document = FetchMyReservationDocument;
    
  }
export const FetchClientCommentsDocument = gql`
    query fetchClientComments {
  fetchClientComments {
    message
    createdAt
    sender {
      _id
      firstName
      lastName
    }
    destinataire {
      _id
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchClientCommentsGQL extends Apollo.Query<FetchClientCommentsQuery, FetchClientCommentsQueryVariables> {
    document = FetchClientCommentsDocument;
    
  }
export const CreateCommentDocument = gql`
    mutation createComment($commentDto: CommentDto!) {
  createComment(commentInput: $commentDto) {
    message
    createdAt
    sender {
      _id
      firstName
      lastName
    }
    destinataire {
      _id
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateCommentGQL extends Apollo.Mutation<CreateCommentMutation, CreateCommentMutationVariables> {
    document = CreateCommentDocument;
    
  }
export const FetchCategoriesDocument = gql`
    query FetchCategories {
  fetchCategories {
    records {
      _id
      name
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchCategoriesGQL extends Apollo.Query<FetchCategoriesQuery, FetchCategoriesQueryVariables> {
    document = FetchCategoriesDocument;
    
  }
export const CreateCategoryDocument = gql`
    mutation CreateCategory($categoryInput: CategoryInput!) {
  createCategory(categoryInput: $categoryInput) {
    _id
    name
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateCategoryGQL extends Apollo.Mutation<CreateCategoryMutation, CreateCategoryMutationVariables> {
    document = CreateCategoryDocument;
    
  }
export const FetchEventsDocument = gql`
    query FetchEvents {
  fetchEvents {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        md
        lg
      }
      locationAccuracy
      startDate
      catchyPhrase
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchEventsGQL extends Apollo.Query<FetchEventsQuery, FetchEventsQueryVariables> {
    document = FetchEventsDocument;
    
  }
export const FetchCategoryEventsDocument = gql`
    query FetchCategoryEvents($categoryId: ID!) {
  fetchCategoryEvents(categoryId: $categoryId) {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      locationAccuracy
      description
      startDate
      catchyPhrase
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchCategoryEventsGQL extends Apollo.Query<FetchCategoryEventsQuery, FetchCategoryEventsQueryVariables> {
    document = FetchCategoryEventsDocument;
    
  }
export const FetchRecentEventsDocument = gql`
    query FetchRecentEvents {
  fetchRecentEvents {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      locationAccuracy
      startDate
      catchyPhrase
      state
      status
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchRecentEventsGQL extends Apollo.Query<FetchRecentEventsQuery, FetchRecentEventsQueryVariables> {
    document = FetchRecentEventsDocument;
    
  }
export const ChangeEventStatusDocument = gql`
    mutation ChangeEventStatus($eventId: ID!, $status: EventStatus!) {
  changeEventStatus(eventId: $eventId, status: $status) {
    _id
    name
    tickets {
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ChangeEventStatusGQL extends Apollo.Mutation<ChangeEventStatusMutation, ChangeEventStatusMutationVariables> {
    document = ChangeEventStatusDocument;
    
  }
export const ChangeEventStateDocument = gql`
    mutation ChangeEventState($eventId: ID!, $state: EventState!) {
  changeEventState(eventId: $eventId, state: $state) {
    _id
    name
    tickets {
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ChangeEventStateGQL extends Apollo.Mutation<ChangeEventStateMutation, ChangeEventStateMutationVariables> {
    document = ChangeEventStateDocument;
    
  }
export const FetchEventDocument = gql`
    query FetchEvent($eventId: ID!) {
  fetchEvent(eventId: $eventId) {
    _id
    name
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    address
    locationAccuracy
    startDate
    endDate
    catchyPhrase
    description
    catchyPhrase
    createdBy {
      _id
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchEventGQL extends Apollo.Query<FetchEventQuery, FetchEventQueryVariables> {
    document = FetchEventDocument;
    
  }
export const FetchUpActivePubsDocument = gql`
    query FetchUpActivePubs {
  fetchUpActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchUpActivePubsGQL extends Apollo.Query<FetchUpActivePubsQuery, FetchUpActivePubsQueryVariables> {
    document = FetchUpActivePubsDocument;
    
  }
export const FetchNoUpActivePubsDocument = gql`
    query FetchNoUpActivePubs {
  fetchNoUpActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchNoUpActivePubsGQL extends Apollo.Query<FetchNoUpActivePubsQuery, FetchNoUpActivePubsQueryVariables> {
    document = FetchNoUpActivePubsDocument;
    
  }
export const FetchNextNoUpPubsDocument = gql`
    query FetchNextNoUpPubs {
  fetchNextNoUpPubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchNextNoUpPubsGQL extends Apollo.Query<FetchNextNoUpPubsQuery, FetchNextNoUpPubsQueryVariables> {
    document = FetchNextNoUpPubsDocument;
    
  }
export const FetchActivePubsDocument = gql`
    query FetchActivePubs {
  fetchActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchActivePubsGQL extends Apollo.Query<FetchActivePubsQuery, FetchActivePubsQueryVariables> {
    document = FetchActivePubsDocument;
    
  }
export const FetchNextPubsDocument = gql`
    query FetchNextPubs {
  fetchNextPubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchNextPubsGQL extends Apollo.Query<FetchNextPubsQuery, FetchNextPubsQueryVariables> {
    document = FetchNextPubsDocument;
    
  }

export const FetchCurrentUser = gql`
    query FetchCurrentUser {
  fetchCurrentUser {
    _id
    firstName
    lastName
    email
    role
    avatar {
      sm
      lg
      md
    }
    cards {
      _id
      number
    }
  }
}
    `;
export const Register = gql`
    mutation Register($registerInput: RegisterDto!) {
  register(registerInput: $registerInput) {
    token
    user {
      _id
      firstName
      lastName
      phoneNumber
      email
      avatar {
        md
      }
    }
  }
}
    `;
export const Login = gql`
    query Login($loginInput: LoginDto!) {
  login(loginInput: $loginInput) {
    token
    user {
      _id
      firstName
      lastName
      phoneNumber
      email
      avatar {
        md
      }
    }
  }
}
    `;
export const UpdatePassword = gql`
    mutation updatePassword($updatePasswordDto: UpdatePasswordDto!) {
  updatePassword(updatePasswordDto: $updatePasswordDto)
}
    `;
export const ResetPassword = gql`
    query resetPassword($email: String!) {
  resetPassword(email: $email)
}
    `;
export const CreateCard = gql`
    mutation CreateCard($cardNumber: String!) {
  createCard(cardNumber: $cardNumber) {
    number
  }
}
    `;
export const FetchMyCards = gql`
    query FetchMyCards {
  fetchMyCards {
    _id
    number
  }
}
    `;
export const LostCard = gql`
    mutation lostCard($cardNumber: String!, $emergencyCard: String) {
  lostCard(cardNumber: $cardNumber, emergencyCard: $emergencyCard) {
    number
  }
}
    `;
export const CreateReservation = gql`
    mutation createReservation($reservationInput: ReservationInput!) {
  createReservation(reservationInput: $reservationInput)
}
    `;
export const FetchMyReservation = gql`
    query FetchMyReservation {
  fetchMyReservations {
    event {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      status
      state
      address
      locationAccuracy
      startDate
      endDate
      catchyPhrase
      description
      catchyPhrase
      createdBy {
        firstName
        lastName
      }
    }
    ticket {
      name
      price
    }
    card {
      number
    }
  }
}
    `;
export const FetchClientComments = gql`
    query fetchClientComments {
  fetchClientComments {
    message
    createdAt
    sender {
      _id
      firstName
      lastName
    }
    destinataire {
      _id
      firstName
      lastName
    }
  }
}
    `;
export const CreateComment = gql`
    mutation createComment($commentDto: CommentDto!) {
  createComment(commentInput: $commentDto) {
    message
    createdAt
    sender {
      _id
      firstName
      lastName
    }
    destinataire {
      _id
      firstName
      lastName
    }
  }
}
    `;
export const FetchCategories = gql`
    query FetchCategories {
  fetchCategories {
    records {
      _id
      name
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;
export const CreateCategory = gql`
    mutation CreateCategory($categoryInput: CategoryInput!) {
  createCategory(categoryInput: $categoryInput) {
    _id
    name
  }
}
    `;
export const FetchEvents = gql`
    query FetchEvents {
  fetchEvents {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        md
        lg
      }
      locationAccuracy
      startDate
      catchyPhrase
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;
export const FetchCategoryEvents = gql`
    query FetchCategoryEvents($categoryId: ID!) {
  fetchCategoryEvents(categoryId: $categoryId) {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      locationAccuracy
      description
      startDate
      catchyPhrase
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;
export const FetchRecentEvents = gql`
    query FetchRecentEvents {
  fetchRecentEvents {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      locationAccuracy
      startDate
      catchyPhrase
      state
      status
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;
export const ChangeEventStatus = gql`
    mutation ChangeEventStatus($eventId: ID!, $status: EventStatus!) {
  changeEventStatus(eventId: $eventId, status: $status) {
    _id
    name
    tickets {
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;
export const ChangeEventState = gql`
    mutation ChangeEventState($eventId: ID!, $state: EventState!) {
  changeEventState(eventId: $eventId, state: $state) {
    _id
    name
    tickets {
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;
export const FetchEvent = gql`
    query FetchEvent($eventId: ID!) {
  fetchEvent(eventId: $eventId) {
    _id
    name
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    address
    locationAccuracy
    startDate
    endDate
    catchyPhrase
    description
    catchyPhrase
    createdBy {
      _id
      firstName
      lastName
    }
  }
}
    `;
export const FetchUpActivePubs = gql`
    query FetchUpActivePubs {
  fetchUpActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchNoUpActivePubs = gql`
    query FetchNoUpActivePubs {
  fetchNoUpActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchNextNoUpPubs = gql`
    query FetchNextNoUpPubs {
  fetchNextNoUpPubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchActivePubs = gql`
    query FetchActivePubs {
  fetchActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchNextPubs = gql`
    query FetchNextPubs {
  fetchNextPubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

      export interface IntrospectionResultData {
        __schema: {
          types: {
            kind: string;
            name: string;
            possibleTypes: {
              name: string;
            }[];
          }[];
        };
      }
      const result: IntrospectionResultData = {
  "__schema": {
    "types": []
  }
};
      export default result;
    
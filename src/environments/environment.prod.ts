export const environment = {
  production: true,
  API_URL: "https://clavem-api.herokuapp.com",
  GRAPHQL_URI: "https://clavem-api.herokuapp.com/graphql",
};
